package com.aytycap.a2cap3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.text = (TextView) findViewById(R.id.welcome);
        Bundle bundle = getIntent().getExtras();
        String mensage = bundle.getString("nome");
        this.text.setText("Welcome! " + mensage);
    }
}
