package com.aytycap.a2cap3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText mLogin; //m inicial de modelo por padrão.
    private EditText mPassword;
    private Button mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.mLogin = (EditText) findViewById(R.id.login);
        this.mPassword = (EditText) findViewById(R.id.password);
        this.mBtn = (Button) findViewById(R.id.btn);
        this.mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logar();
            }
        });
    }
    private void logar(){
        String login = mLogin.getText().toString();
        String password = mPassword.getText().toString();
        if (login.equalsIgnoreCase("Jeferson")&& password.equalsIgnoreCase("123")){
            Intent intent = new Intent(this,MainActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("nome",login);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else{
            View focus = null;
            if (TextUtils.isEmpty(login)){
                this.mLogin.setError("Login incorect!");
                focus = this.mLogin;
                focus.requestFocus();
            }
            if (TextUtils.isEmpty(password)){
                this.mPassword.setError("Password incorect!!");
                focus = this.mPassword;
                focus.requestFocus();
            }
            else{
                this.mLogin.setError("Login and Password incorect!");
                focus = this.mLogin;
                focus.requestFocus();
            }
        }
    }
}
